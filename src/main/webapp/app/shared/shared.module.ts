import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FitnessSharedLibsModule, FitnessSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [FitnessSharedLibsModule, FitnessSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [FitnessSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FitnessSharedModule {
  static forRoot() {
    return {
      ngModule: FitnessSharedModule
    };
  }
}
